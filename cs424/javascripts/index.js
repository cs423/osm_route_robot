var lat, lng;
var map, mode, radius;
var API_ENDPOINT, CLIENT_ID, CLIENT_SECRET;

window.addEventListener('load', getLocation);

// code for html5 geolocation
function getLocation()
{
    if (navigator.geolocation)
    {
        // timeout at 10 seconds or 10000 milliseconds
        console.log("Looks like i got the location!");
        var timelimit = {
            timeout: 10000
        };
        navigator.geolocation.getCurrentPosition(showPosition, showError, timelimit);
    }
    else
    {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

// show the user's location
function showPosition(position)
{
    lat = 40.7143528;
    lng = -74.0059731;

    console.log("function: showPosition: Latitude: " + lat + " Longitude: " + lng);
    prepareMap(lat, lng);
}

// show the default location
function showError(error)
{
    lat = 40.7143528;
    lng = -74.0059731;

    console.log("function: showError: Latitude: " + lat + " Longitude: " + lng);
    prepareMap(lat, lng);
}



// pre-condition: places is an array of stings, tag is a string
function match_input(places, tag)
{
    var lowcase_places = new Array();
    var entry = -1;
    //turn the input array and the tag into lowcase
    for (var i = 0; i < places.length; i++)
    {
        lowcase_places[i] = places[i].toLowerCase();
    }
    var lowcase_tag = tag.toLowerCase();

    //start to match 
    //console.log(lowcase_places);
    // console.log(tag);
    var i;
    for (i = 0; i < lowcase_places.length; i++)
    {

        if (lowcase_tag.indexOf(lowcase_places[i]) >= 0)
        {
            // if we found the substring
            return i;
        }
    }
    // entry is where the tag should fit in
    return entry;
}

function group(venues, places)
{
    //console.log(venues.length);
    var osm_array = new Array();
    var cate_num = places.length;
    var lat_lng = '';
    var lat;
    var lng;
    var threshold = 5;

    //initiate a an array of arrays
    for (var i = 0; i < cate_num; i++)
    {
        osm_array[i] = new Array();
    }

    var result = -1;
    for (var i = 0; i < venues.length; i++)
    {
        result = match_input(places, venues[i].categories[0].name);
        //console.log("matching: "+places+" with "+venues[i].categories[0].name+": "+result);

        //console.log(result);
        if (result != -1 && osm_array[result].length< threshold)
        {
            lat = venues[i].location.lat;
            lng = venues[i].location.lng;
            lat_lng = lat_lng.concat(lat, ',', lng);
            osm_array[result].push(
                lat_lng
            );
            lat_lng = '';
        }
    }
    return osm_array;
}



function prepareMap(lat, lng)
{
    console.log("function: prepareMap: Latitude: " + lat + " Longitude: " + lng);

    lat = 40.7143528;
    lng = -74.0059731;

    var latlng = [lat, lng];

    console.log("latlng: " + latlng[0] + " " + latlng[1]);

    L.mapbox.accessToken = 'pk.eyJ1Ijoic3VrZXR1OTMiLCJhIjoiVDRNNndyTSJ9.5oW805KlbqhxiCsqoxnDiA';
    map = L.mapbox.map('map', 'suketu93.l62ii624').setView(latlng, 13);

    // Credit Foursquare for their wonderful data
    map.attributionControl.addAttribution('<a href="https://foursquare.com/">Places data from Foursquare</a>');

    // Create a Foursquare developer account: https://developer.foursquare.com/
    CLIENT_ID = 'QGPZS0XNT5CHNLUJYLXV35VRKEE35WPMYIOSJQDCPPPHVSCY';
    CLIENT_SECRET = 'KXO0YJHHZVSEFNZIRURWHTVYOZXEVTLZJQAOE0QJ0LZXJPVM';

    var myLayer = L.mapbox.featureLayer().addTo(map);

    myLayer.setGeoJSON(
    {
        type: 'Feature',
        geometry:
        {
            type: 'Point',
            coordinates: [lng, lat]
        },
        properties:
        {
            'title': 'Here I am!',
            'marker-color': '#FE2E2E',
            'marker-symbol': 'star'
        }
    });
    // map.fitBounds(foursquarePlaces.getBounds());
}

// combination.js
function str2arr(str)
{
    var arr = new Array();
    arr = str.split("/");
    return arr;
}

function cross_product(list1, list2)
{
    var result = new Array();
    var pointer = 0;
    var length1 = list1.length;
    var length2 = list2.length;
    for (var i = 0; i < length1; i++)
    {

        for (var j = 0; j < length2; j++)
        {
            //result[pointer]= new Array();
            //result[pointer] = [list1[i],"/",list2[j]];
            result[pointer] = list1[i].concat("/", list2[j]);
            pointer++;
        }
    }
    return result;
}

function combination(M)
{
    var r = new Array();
    r = M[0];
    for (var i = 1; i < M.length; i++)
    {
        r = cross_product(r, M[i]);
    }

    for (var i = 0; i < r.length; i++)
    {
        r[i] = str2arr(r[i]);
    }
    return r;
}

// once the page is ready
$(document).ready(function()
{
    var dests = $('#destinations');
    var i = $('#destinations li').length;

    // adding destinations dynamically
    $('#add_dest').click(function()
    {
        if (i < 9)
        {
            i++;
            dests.append('<div class="input-group"><input type="text" class="form-control"><span class="input-group-btn"><button class="btn btn-danger remove_dest" type="button" data-id="' + i + '">&times;</button></span></div>');
        }
        else
        {
            $("#max_dest").removeClass("hidden");
        }
        return false;
    });

    // removing destinations
    $(document).on('click', '.remove_dest', function()
    {
        var id = $(this).data("id");
        $("*[data-id=" + id + "]").parent().parent().remove();
        i--;
        $("#max_dest").addClass("hidden");
    });

    var list = new Array();
    var t = 0;

    var foursquarePlaces = new L.LayerGroup();

    $('#calculate').click(function(e)
    {
        e.preventDefault();
        //add the first chunk showing origin info
        var origin = $('#origin'); 
        origin.after('<div class="well well-sm"><span class="label label-danger">origin</span> <div>[address information]</div></div>');
        //    
        var dests = $('#destinations');
        var i = $('#destinations li').length;

        // get the transportation mode type from html
    /*    if (document.getElementById('driving').checked)
        {
            mode = document.getElementById('driving').value;
        }
        else if (document.getElementById('walking').checked)
        {
            mode = document.getElementById('walking').value;
        }
        else if (document.getElementById('bicycle').checked)
        {
            mode = document.getElementById('bicycle').value;
        }*/

        // get the radius from html
        var sel = document.getElementById('radius');
        radius = sel.options[sel.selectedIndex].value;

        // CONVERT radius from miles to meters
        console.log("Radius in miles: " + radius);
        radius = radius / 0.00062137;
        console.log("Radius: " + radius);

        // keywords from the map
        var items = dests.find("input");

        var index = 0;
        var places = new Array();

        for (var v = 0; v < items.length; v++)
        {
            var val = $(items[v]).val();
            console.log("index: " + index + " value: " + val);
            places[index++] = val;
        }

        if (foursquarePlaces.layerGroup)
        {
            foursquarePlaces.clearLayers();
        }

        // Keep our place markers organized in a nice group.
        foursquarePlaces.addTo(map);

        var count = 0;
        var marker_color = new Array();
        var response_counter = 0;

        // Use jQuery to make an AJAX request to Foursquare to load markers data.
        for (var v = 0; v < places.length; v++)
        {
            var keyword = places[v];
            console.log("keyword: " + keyword);
            if(v == 0)
            {
                // initiate the stops well
                var first_stop = $('#stops');
                first_stop.after('<div class="well well-sm"><span class="label label-success">Stop [N]</span><div>[address information]<ul id ="stops_hook"></ul></div></div>');
            }
            else
            {
                // append on the stop hook
                var other_stops = $('#stops_hook');
                other_stops.after('<div class="label label-success">Stop [N]</div><div>[address information]<ul id ="stops_hook"></ul></div>');
            }

            // https://developer.foursquare.com/start/search
            API_ENDPOINT = 'https://api.foursquare.com/v2/venues/search' +
                '?client_id=CLIENT_ID' +
                '&client_secret=CLIENT_SECRET' +
                '&v=20130815' +
                '&ll=LATLON' +
                '&query=KEYWORD' +
                '&radius=RADIUS' +
                '&callback=?';

            $.getJSON(API_ENDPOINT
                .replace('CLIENT_ID', CLIENT_ID)
                .replace('CLIENT_SECRET', CLIENT_SECRET)
                .replace('KEYWORD', keyword)
                .replace('RADIUS', radius)
                .replace('LATLON', lat +
                    ',' + lng),
                function(result, status)
                {
                    if (status !== 'success')
                        return alert('Request to Foursquare failed');

                    response_counter++;

                    // Transform each venue result into a marker on the map.
                    for (var i = 0; i < result.response.venues.length; i++)
                    {
                        var venue = result.response.venues[i];
                        var latlng = L.latLng(venue.location.lat, venue.location.lng);
                        //var m = classify(venue);

                        //console.log('this is a '+venue.categories[0].name);
                        // console.log(venue);
                        list[t] = venue;
                        t++;

                        /*
                        var marker = L.marker(latlng,
                            {
                                icon: L.mapbox.marker.icon(
                                {
                                    //'marker-color': '#0000FF',
                                    'marker-color': m.color,
                                    //'marker-symbol': 'marker'
                                    'iconUrl': venue.categories[0].icon.prefix + 'bg_32' + venue.categories[0].icon.suffix,
                                    'marker-symbol': m.tag
                                })
                            })
                            .bindPopup('<strong><a href="https://foursquare.com/v/' + venue.id + '">' +
                                venue.name + '</a></strong>')
                            .addTo(foursquarePlaces);
                        */
                        var myIcon = L.icon({
                        iconUrl: venue.categories[0].icon.prefix + 'bg_32' + venue.categories[0].icon.suffix,
                        iconSize: [22, 22],
                    });

                    L.marker([venue.location.lat, venue.location.lng], {icon: myIcon }).bindPopup('<strong><a href="https://foursquare.com/v/' + venue.id + '">' +
                                venue.name + '</a></strong>').addTo(foursquarePlaces);


                        /* for later use, display the list of icons */
                        var icons = $('#icons');
                        icons.append('<span><img src="' + venue.categories[0].icon.prefix + 'bg_32' + venue.categories[0].icon.suffix + '"</span>');
                        //icons.append('<span><img src="https://foursquare.com/img/categories_v2/food/icecream_bg_32.png"</span>');
                    }

                    if (response_counter == places.length)
                    {
                        //console.log(list);
                        //console.log(places);
                        //console.log(result.response.venues.length);
                        var osm_array = group(list, places);
                        //console.log(osm_array);
                        /*
                            // parsing the client input
                            for(var i=0; i<osm_array.length; i++)
                            {
                                for(var j=0; j<osm_array[i].length; j++)
                                {
                                    console.log("inside here");
                                    console.log(osm_array[i][j]);
                                }
                            }
                        */
                        console.log("osm_array: ")
                        console.log(osm_array);
                        var combos = combination(osm_array);
                        console.log("combos: ")
                        console.log(combos);
                        var host = "localhost";
                        var port = "5000";
                        // TODO: parse combos and for each combo in combos, pass it to the distance table

                        combos.forEach(function(combo) {
                            var path = "/table?"
                            for(var i = 0; i < combo.length; i++) {
                                if(i == 0) {
                                    path += "loc=" + combo[i];
                                }
                                else {
                                    path += "&loc=" + combo[i];
                                }
                            }
                            var theUrl = "http://" + host + ":" + port + path;
                            console.log(theUrl);

                            var dmJson = null;
                            dmJson = new XMLHttpRequest();
                            dmJson.open("GET", theUrl, true);
                            dmJson.send();
                            console.log(dmJson);
                        });

                        // TODO: parse combos and for each combo in combos, pass it to the distance table

                        // Distance Table
                        /*var http = require('http');
                        var options = {
                            host: 'localhost',
                            port: '5000',
                            path: '/table?loc=40.905087,-73.2312&loc=40.9050,-73.231239&loc=40.9050,-73.2312'
                        };
                        console.log(foursquare_response);
                    /*  arr.forEach(function(combo) {
                            distanceMatrix(combo, mode, DM.length);
                        });

                        var request = http.get(options, function(result) {
                            //console.log('STATUS: ' + res.statusCode);
                            // console.log('HEADERS: ' + JSON.stringify(res.headers));

                            // Buffer the body entirely for processing as a whole.
                            var bodyChunks = [];
                            result.on('data', function(chunk) {
                            // You can process streamed parts here...
                            bodyChunks.push(chunk);
                            }).on('end', function() {
                                var body = Buffer.concat(bodyChunks);
                                console.log('BODY: ' + body);
                                // ...and/or process the entire body here.
                            })
                        });

                        request.on('error', function(e) {
                          console.log('ERROR: ' + e.message);
                        });*/

                        /*$.ajax(
                        {
                            url: '/data',
                            data: combos,
                            type: 'POST',
                            success: function(data)
                            {
                                //var ret = jQuery.parseJSON(data);
                                //$('#lblResponse').html(ret.msg);
                                console.log('Success: ')
                            },
                            error: function(xhr, status, error)
                            {
                                console.log('Error: ' + error.message);
                                //$('#lblResponse').html('Error connecting to the server.');
                            },
                        });*/
                    }
                });
        };
    });
});