var express = require('express');
var request = require("request");
var server = express.Router();

server.use(function(req, res, next) {
	console.log(req.method, req.url);
	next();
}); 

server.post('/', function(req, res) {

	var param = req.body;

	console.log(param);
	
	/* get the object passed by the client's post request */
	request(param.url, function(error, response, body) {
		/* send the confirmation back to the client */
		console.log(body);
		res.send(body);
	});
});

module.exports.server = server;