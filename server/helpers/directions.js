var config = require("./config.js");
var https = require("https");

function getDirections() {
  var body = "";
	var json = "";
	var url = "https://maps.googleapis.com/maps/api/directions/json?origin=Adelaide,SA&destination=Adelaide,SA&waypoints=optimize:true|Barossa+Valley,SA|Clare,SA|Connawarra,SA|McLaren+Vale,SA&key=" + config.apiKey;
  https.get(url, function(response) { 
        response.on('data', function (chunk) {
            body += chunk;
        });
        response.on('end', function () {
            json = JSON.parse(body);
            console.log(json.routes[0].waypoint_order);
        });
    });

}

getDirections();