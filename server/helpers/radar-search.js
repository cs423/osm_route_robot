var conf = require('./config.js');
var https = require("https");

exports.getDirections = function (location, radius, keyword) {
    var body = "";
    var json = ""; 
    var lat = "";
    var lng = "";
    var arr = [];
    var url = "https://maps.googleapis.com/maps/api/place/radarsearch/json?location="+ location +"&radius="+ radius +"&keyword="+ keyword + "&key="+conf.apiKey;
    https.get(url, function(response) { 
        response.on('data', function (chunk) {
            body += chunk;
        });
        response.on('end', function () {
            json = JSON.parse(body);
            for(var name in json.results){
                lat = json.results[name].geometry.location.lat;
                lng = json.results[name].geometry.location.lng;
                var latlng = lat + "," + lng;
                arr.push(latlng);
            }
            console.log(arr);
        });
    });
}
