
var min_cost=11111111;
var solution = new Array();
var permutation =function(list,  k,  m,  DM) 
{ // generate all permutation of the list
    // list: all the places in the current combo , immutable
    // k : the recursion controler
    // m:  the recursion controler, immutable
    // DM: the distance matrix with two values--------place list and the table
    // min: records the minimum value of the route cost, updated when a cost is lower than it

    var i; 
    var sum=0;
    var from;
    var to;
    //var solution = new Array();
    
    if (k == m) { 
      // to traverse the list
        for (i = 0; i <= m; i++)  
            for(var j=0,sum=0; j< list.length-1; j++)
              {
                from = search(list[j], DM);
                to= search(list[j+1], DM);
                sum += DM.table[from][to];
                }
            // TO SUM UP THE COST!!!!!
            //console.log("sum is: ",sum);

        if(sum < min_cost)
        {
          min_cost = sum;
          //console.log("min_cost updated!");
          //console.log("min_cost: ",min_cost);
          // copy the current list into the solution array
          //solution = list;
          solution = list.slice(0);
        }
        
    
    } 
    else  
      for (i=k; i <= m; i++) {  
      var temp;
      //swap list[k] and list[i];
      temp = list[k];
      list[k] = list[i];
      list[i] = temp;
             
      permutation(list, k+1, m, DM); 
      
      //do the swap again
      temp = list[k];
      list[k] = list[i];
      list[i] = temp; 
      } 

      // return our optimal solution (an array)
      //return min_cost;
      return solution;
}

function search(place, DM)
{
  var i=0;
  for(i =0; i< DM.places.length; i++)
  {
    if(DM.places[i] == place)
      return i;
  }

}

module.exports.permutation = permutation;
module.exports.min_cost= min_cost;

