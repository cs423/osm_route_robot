var combination = function(M)
{
  var r = new Array();
  r= M[0];
   for(var i=1;i<M.length;i++)
  {
    r = cross_product(r,M[i]);
  }

  for(var i=0; i<r.length; i++)
  {
    r[i] = str2arr(r[i]);
  }
  return r;
}


function str2arr(str)
{
  var arr = new Array();
  arr = str.split("/");
  return arr;
}


function cross_product(list1,list2)
{
  var result = new Array();
  var pointer = 0;
  var length1 = list1.length;
  var length2 = list2.length;
  for(var i =0 ; i< length1; i++)
  {

      for(var j=0; j<length2;j++)
      {
        //result[pointer]= new Array();
        //result[pointer] = [list1[i],"/",list2[j]];
        result[pointer] = list1[i].concat("/",list2[j]);
        pointer++;
      }
  }
  return result;
}

module.exports.combination = combination;