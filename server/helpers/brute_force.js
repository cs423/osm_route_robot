// JavaScript Document
//
//document.write(”<script language=javascript src=’/distancematrix.js’>
//<script src="distancematrix.js">
var minimum=0;
function Perm( list,  k,  m) 
{ // generate all permutation of the list
    var outputDiv = document.getElementById('outputDiv');
    var i;
    
    if (k == m) { 
        for (i = 0; i < m; i++) 
            outputDiv.innerHTML += list[i]+' '; 
		outputDiv.innerHTML += list[i]+':   ';
        
		var result=  0;
		
		for(var j=0; j< list.length; j++)
			{
				if(j< list.length-1)
				result = result+ DM(list[j],list[j+1]);
			}
			outputDiv.innerHTML += result;
			outputDiv.innerHTML += '<br>';
			if(result< minimum)
				minimum = result;
    } 
    else  
        for (i=k; i <= m; i++) {  
			var temp;
			//swap list[k] and list[i];
			temp = list[k];
			list[k] = list[i];
			list[i] = temp;
             
            Perm (list, k+1, m); 
			
			
            temp = list[k];
			list[k] = list[i];
			list[i] = temp; 
        } 
} 


var permArr = [],
    usedChars = [];

function permute(input) {
    var i, ch;
    for (i = 0; i < input.length; i++) {
        ch = input.splice(i, 1)[0];
        usedChars.push(ch);
        if (input.length == 0) {
            permArr.push(usedChars.slice());
        }
        permute(input);
        input.splice(i, 0, ch);
        usedChars.pop();
    }
    return permArr
};


console.log( permute(["B", "C", "D"]));
