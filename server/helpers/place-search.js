var config = require("./config.js");
var Promise = require('bluebird');
var DistanceMatrix = require("./distance-matrix.js");
var GooglePlaces = Promise.promisifyAll(require("googleplaces"));
var googlePlaces = new GooglePlaces(config.apiKey, config.outputFormat);
var extract = require('./extract.js');
var combination = require('./combination.js');
var permutation = require('./permutation.js');

function placeSearch(obj) {

    console.log("Inside place search!");

    /**
     * Place search - https://developers.google.com/places/documentation/#PlaceSearchRequests
     */
    var arr = [];
    var count = 0;

    // PLACE SEARCH ACCEPTS RADIUS IN METERS! WE NEED TO FIX THIS.
    var rad = obj["radius"];
    var arrr = new Array();

    console.log("radius: " + rad);
    var loc = obj["location"];
    console.log("Location: " + loc);
    var mode = obj["mode"];

    var params = obj["params"];

    /* client's keywords */
    var arr;
    var ar = [];
    for (var i = 0; i < params; i++) {
        arr[i] = obj[i];
        //console.log(arr[i]);
        var param = {
            location: loc,
            radius: rad,
            mode: mode,
            keyword: arr[i]
        };
        ar.push(param);
    }

    console.log("before promises");
    
    var promises = ar.map(function(name) {
        return googlePlaces.placeSearch(name, function(response) {

            arrr.push(response);
            //console.log(response);
            //console.log(arrr);
            count++;
            //console.log(count++);
            //If all responses have been returned
            //Find combos and pass to distance-matrix
            if (count == ar.length) {
                //console.log(arrr);
                var Matrix = new Array();
                var result = new Array();

                //for(var i=0; i<arr.length ; i++)
                //console.log(arr[i]);
                //to extract only lat and lng from arr.results
                Matrix = extract.extract(arrr);
                result = combination.combination(Matrix);

                // NOW RESULT IS THE ARRAY OF ALL COMBINATION

                // NOW RESULT IS THE ARRAY OF COMBINATIONS OF latlng pairs AND PASS IT TO FRONTEND
                result.forEach(function(combo, index) {
                    //console.log("combo" + combo)
                    DistanceMatrix.distanceMatrix(mode, combo, result.length);
                });


                // IF YOU WANT TO SEE PERMUTATION
                //permutation.permutation(result);

                console.log("combination results: " + result);

            }

        })
    });

}

module.exports.placeSearch = placeSearch;