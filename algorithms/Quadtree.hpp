#ifndef __QUADTREE_H__
#define __QUADTREE_H__

#include <vector>
#include "Object.h"

using namespace std;

class Quadtree
{

public:
	Quadtree(float latitude, float longitude, float width, float height, int level, int maxLevel);
	~Quadtree();
	
	void 		addObject(Object* obj);
	void 		clear();
	vector<Object*>	getObjectAt(float x, float y);

private:
	const int	maxObjectsPerLevel = 2;
	float 		qLatitude;
	float 		qLongitude;
	float		qWidth;
	float		qHeight;
	int		qLevel;
	int 		qMaxLevel;
	
	vector<Object*>	objects;
	
	Quadtree*	parent;
	Quadtree* 	TR;
	Quadtree* 	TL;
	Quadtree* 	BR;
	Quadtree* 	BL;
	
	bool contains(Quadtree* child, Object* obj);
};

#endif


// js version
