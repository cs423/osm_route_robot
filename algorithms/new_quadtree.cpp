#include <node.h>

using namespace v8;

// create new quadtree
Handle<Value> CreateQuadtree(const Arguments& args) {
	HandleScope scope;

	Local<Quadtree> quadtree = new Quadtree(args[0], 
											args[1],
											args[2],
											args[3],
											args[4],
											args[5]);

	return scope.Close(quadtree);
}

// initialize other functions
void Init(Handle<Object> exports) {
	Quadtree::Quadtree(exports);
}

NODE_MODULE(quadtree, Init)