#ifndef __OBJECT_H__
#define __OBJECT_H__

class Object{

public:
	float oLatitude;
	float oLongitude;
	
	Object(float latitude, float longitude)
	{
		oLatitude = latitude;
		oLongitude = longitude;
	}
};

#endif


// js version
#define BUILDING_NODE_EXTENSION
#include <node.h>

using namespace v8;

Handle<Value> CreateObject(const Arguments& args) {
  HandleScope scope;

  Local<Object> Object = Object::New();
  Object->Set(String::NewSymbol("msg"), args[0]->ToString());

  return scope.Close(obj);
}

void Init(Handle<Object> exports, Handle<Object> module) {
  module->Set(String::NewSymbol("exports"),
      FunctionTemplate::New(CreateObject)->GetFunction());
}

NODE_MODULE(addon, Init)