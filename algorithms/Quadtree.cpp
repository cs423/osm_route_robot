#include <vector>
#include <node.h>
#include <v8.h>

using namespace v8;

class Object{

public:
	float oLatitude;
	float oLongitude;
	
	Object(float latitude, float longitude)
	{
		oLatitude = latitude;
		oLongitude = longitude;
	}
};

class Quadtree
{

public:
	Quadtree(float latitude, float longitude, float width, float height, int level, int maxLevel);
	~Quadtree();
	
	void 		addObject(Object* obj);
	void 		clear();
	vector<Object*>	getObjectAt(float x, float y);

private:
	const int	maxObjectsPerLevel = 2;
	float 		qLatitude;
	float 		qLongitude;
	float		qWidth;
	float		qHeight;
	int		qLevel;
	int 		qMaxLevel;
	
	vector<Object*>	objects;
	
	Quadtree*	parent;
	Quadtree* 	TR;
	Quadtree* 	TL;
	Quadtree* 	BR;
	Quadtree* 	BL;
	
	bool contains(Quadtree* child, Object* obj);
};

Quadtree::Quadtree(float latitude, float longitude, float width, float height, int level, int maxLevel)
{
	qLatitude 	= latitude;
	qLongitude 	= longitude;
	qWidth		= width;
	qHeight		= height;
	qLevel 		= level;
	qMaxLevel 	= maxLevel;
	
	if(qLevel == qMaxLevel)
	{
		return;
	}
	
	TL = new Quadtree(qLatitude, qLongitude, qWidth / 2.0f, qHeight / 2.0f, qLevel + 1, qMaxLevel);
	TR = new Quadtree(qLatitude + qWidth / 2.0f, qLongitude, qWidth / 2.0f, qHeight / 2.0f, qLevel + 1, qMaxLevel);
	BL = new Quadtree(qLatitude, qLongitude + qHeight / 2.0f, qWidth / 2.0f, qHeight / 2.0f, qLevel + 1, qMaxLevel);
	BR = new Quadtree(qLatitude + qWidth / 2.0f, qLongitude + qHeight / 2.0f, qWidth / 2.0f, qHeight / 2.0f, qLevel + 1, qMaxLevel);
}

Quadtree::~Quadtree()
{
	if(qLevel == qMaxLevel)
	{
		return;
	}
	
	delete TR;
	delete TL;
	delete BR;
	delete BL;
}

void Quadtree::addObject(Object* obj)
{
	if(qLevel == qMaxLevel)
	{
		objects.push_back(obj);
		return;
	}

	if(contains(TL, obj))
	{
		TL->addObject(obj);
		return;
	}
	else if(contains(TR, obj))
	{
		TR->addObject(obj);
		return;
	}
	else if(contains(BL, obj))
	{
		BL->addObject(obj);
		return;
	}
	else if(contains(BR, obj))
	{
		BR->addObject(obj);
		return;
	}

	if(contains(this, obj))
	{
		objects.push_back(obj);
	}
}

vector<Object*> Quadtree::getObjectAt(float latitude, float longitude)
{
	if(qLevel == qMaxLevel)
	{
		return objects;
	}

	vector<Object*> returnObjects, childReturnObjects;

	if(!objects.empty())
	{
		returnObjects = objects;
	}

	if(latitude > qLatitude + qWidth / 2.0f && latitude < qLatitude + qWidth)
	{
		if(longitude > qLongitude + qHeight / 2.0f && longitude < qLongitude + qHeight)
		{
			childReturnObjects = BR->getObjectAt(latitude,longitude);
			returnObjects.insert(returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end());
			return returnObjects;
		}
		else if(longitude > qLongitude && longitude <= qLongitude + qHeight / 2.0f)
		{
			childReturnObjects = TR->getObjectAt(latitude,longitude);
			returnObjects.insert(returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end());
			return returnObjects;
		}
	}
	else if(latitude > qLatitude && latitude <= qLatitude + qWidth / 2.0f)
	{
		if(longitude > qLongitude + qHeight / 2.0f && longitude < qLongitude + qHeight)
		{
			childReturnObjects = BL->getObjectAt(latitude,longitude);
			returnObjects.insert(returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end());
			return returnObjects;
		}
		else if(longitude > qLongitude && longitude <= qLongitude + qHeight / 2.0f)
		{
			childReturnObjects = TL->getObjectAt(latitude,longitude);
			returnObjects.insert(returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end());
			return returnObjects;
		}
	}

	return returnObjects;
}

void Quadtree::clear()
{
	if(qLevel == qMaxLevel)
	{
		objects.clear();
		return;
	}
	else
	{
		BR->clear();
		BL->clear();
		TR->clear();
		TL->clear();
	}

	if(!objects.empty())
	{
		objects.clear();
	}
}

bool Quadtree::contains(Quadtree* child, Object* obj)
{
	return !(obj->oLatitude  < child->qLatitude ||
			 obj->oLongitude < child->qLongitude ||
			 obj->oLatitude  > child->qLatitude + child->qWidth  ||
		   	 obj->oLongitude > child->qLongitude + child->qHeight);
}

int main()
{
	return 0;
}