function calcRoute(route) {

	var start_location = {
		location: start_address
	}

	var waypoints = Array();
	route.forEach(function(location) {
		if (location != start_address) {
			var loc = {
				location: location
			};
			waypoints.push(loc);
		}
	});

	var request = {
		origin: latlon,
		destination: latlon,
		waypoints: waypoints,
		optimizeWaypoints: true,
		travelMode: mode
	};

	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
			var final_route = response.routes[0];
			var summaryPanel = document.getElementById("directions_panel");
			summaryPanel.innerHTML = "";

			for (var i = 0; i < route.legs.length; i++) {
				var routeSegment = i+1;

				summaryPanel.innerHTML += "<b>Route Segment: " + routeSegment + "</b><br />";
        		summaryPanel.innerHTML += route.legs[i].start_address + " to ";
        		summaryPanel.innerHTML += route.legs[i].end_address + "<br />";
        		summaryPanel.innerHTML += route.legs[i].distance.text + "<br /><br />";
			}

			summaryPanel.show();
		}
	})
}