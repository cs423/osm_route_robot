var map = L.map('map').setView([40.7450, -74.0300], 13);

// add the base layer for the map 
L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
			maxZoom: 18,
			attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
				'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				'Imagery © <a href="http://mapbox.com">Mapbox</a>',
			id: 'examples.map-i875mjb7'
		}).addTo(map);


//marker and popups
var marker = L.marker([40.7450, -74.0300],{draggable:true}).addTo(map);
marker.bindPopup("I am a popup.").openPopup();

//polygone testing
var polygon = L.polygon([
    [40.7466, -74.0333],
    [40.7466, -74.1323]
]).addTo(map);

//tile layers control testing
var mbUrl = 'https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png';
//var testing = L.mapbox.tileLayer('https://api.tiles.mapbox.com/v3/examples.map-0l53fhk2.json');
var grayscale   = L.tileLayer(mbUrl, {id: 'examples.map-20v6611k'}),
	streets  = L.tileLayer(mbUrl, {id: 'examples.map-i875mjb7'});
var testing = L.tileLayer(mbUrl, {id: 'examples.map-0l53fhk2'});

var baseLayers = {
			"Grayscale": grayscale,
			//"Streets": streets,
			"testing": testing
		};

L.control.layers(baseLayers).addTo(map);
