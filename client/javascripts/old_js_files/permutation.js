var min_cost = 999999999999999;
var solution = new Array();

var permutation = function(list, k, m, DM) { 
// generate all permutation of the list
    // list: all the places in the current combo , immutable
    // k : the recursion controler
    // m:  the recursion controler, immutable
    // DM: the reference distance matrix, immutable
    // min: records the minimum value of the route cost, updated when a cost is lower than it

    var i;
    var sum = 0;
    var from;
    var to;

    if (k == m) {
        // to traverse the list
        for (var j = 0, sum = 0; j < list.length - 2; j++) {
            from = search(list[j], DM);
            to = search(list[j + 1], DM);
            sum += DM.rows[from].elements[to].duration.value;
        }
        // TO SUM UP THE COST!!!!!

        if (sum < min_cost) {
            min_cost = sum;
            //console.log("min_cost: ",min_cost);
            // copy the current list into the solution array
            //solution = list;
        }
    } else {
        for (i = k; i <= m; i++) {
            var temp;
            //swap list[k] and list[i];
            temp = list[k];
            list[k] = list[i];
            list[i] = temp;

            permutation(list, k + 1, m, DM);

            temp = list[k];
            list[k] = list[i];
            list[i] = temp;
        }

        // return our optimal solution (an array)
        //return min_cost;

        solution = list.slice(0);
    }
}

function search(place, DM) {
    var i = 0;
    for (i = 0; i < DM.originAddresses.length; i++) {
        if (DM.originAddresses[i] == place)
            return i;
    }
}