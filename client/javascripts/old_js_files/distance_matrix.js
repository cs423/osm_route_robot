var DMcount = 0;
var solutions = Array();

function distanceMatrix(arr, mode, max) {
  var service = new google.maps.DistanceMatrixService();
  service.getDistanceMatrix(
    {
      origins: arr,
      destinations: arr,
      travelMode: mode,
      avoidHighways: false,
      avoidTolls: false
    }, callback);
  var combos = max;
}

function callback(response, status) {
  if (status == google.maps.DistanceMatrixStatus.OK) {
    
    var origins = response.originAddresses;
    var destinations = response.destinationAddresses;

    for (var i = 0; i < origins.length; i++) {
      var results = response.rows[i].elements;
      for (var j = 0; j < results.length; j++) {
        var element = results[j];
        var distance = element.distance.text;
        var duration = element.duration.text;
        var from = origins[i];
        var to = destinations[j];
      }
    }

    permutation(origins, 0, origins.length, response);
    DMcount++;
    if (DMcount == DM.length) {
      calcRoute(solution.slice(0, solution.length - 1));
    }
  }
  else {
    console.log("INVALID DM RESPONSE: " + status);
  }
}