function extract(map, obj) {
	var place_id = obj.place_id;
		
	var request = {
		placeId: place_id
	};

	var service = new google.maps.places.PlacesService(map);
	var address = '';

	service.getDetails(request, function(place, status) {
		if(status == google.maps.places.PlacesServiceStatus.OK) {
			address = place.formatted_address;
		}
	});

	res_arr[place_id] = address;
}