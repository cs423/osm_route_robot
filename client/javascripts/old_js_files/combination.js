var combination = function(M)
{
  var r = new Array();
  r= M[0];
  for(var i=0; i< M[0].length;i++)
  {
    var temp= new Array();
    temp[0] = M[0][i];
    r[i]= new Array();
    r[i]= temp;
  }
   for(var i=1;i<M.length;i++)
  {
    r = cross_product(r,M[i]);
  }
  // r is a 2D array
  return r;
  
}

// push is the basic operation;

function cross_product(list1,list2)
{
  //prepocess: turn list1 into a 2D array
  var counter = 0;
  var result = new Array();
  var length1 = list1.length;
  var length2 = list2.length;
  for(var i =0 ; i< length1; i++)
  { 
      
      for(var j=0; j<length2;j++)
      {    
        result[counter] = new Array(); 
        //copy everything in list1[i] to result[i]
        for(var k=0;k<list1[i].length;k++)
          result[counter][k] = list1[i][k];

        result[counter].push(list2[j]);
        counter++;
      }
  }
  return result;
}