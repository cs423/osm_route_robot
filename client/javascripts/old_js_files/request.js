var latlon = "40.7450, -74.0300";
var rad = 50;
var mode = "driving";
var map, lat, lng;
var res = new Array();
var start_address;
var tmp = [];

var directionsDisplay;
var directionsService = new google.maps.DirectionsService();

var res_arr = new Array();
var DM = [];

var table_name = 'test';
var input_width = 10;

$( ".autocomplete" ).autocomplete({
  source: [ "pizza", "park", "coffee", "hospital", "hotel", "bar", "laundry" ]
});

// code for html5 geolocation
function getLocation() {
    if (navigator.geolocation) {
        // timeout at 10 seconds or 10000 milliseconds
        console.log("Looks like i got the location!");
        var timelimit = {
            timeout: 10000
        };
        navigator.geolocation.getCurrentPosition(showPosition, showError, timelimit);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

// show the user's location
function showPosition(position) {
    //latlon = position.coords.latitude + "," + position.coords.longitude;
    lat = position.coords.latitude;
    lng = position.coords.longitude;
    latlon = new google.maps.LatLng(lat, lng);
    console.log("Latlon: " + latlon);
    prepareMap(latlon);
}

// show the default location
function showError(error) {
	lat = 40.7450;
	lng = -74.0300;
    latlon = new google.maps.LatLng(lat, lng);
    console.log("Latlon: " + latlon);
    prepareMap(latlon);
}

function prepareMap(latlon) {
	var mapCanvas = document.getElementById('map_canvas');
    directionsDisplay = new google.maps.DirectionsRenderer();
	var mapOptions = {
	    center: latlon,
	    zoom: 15,
	    scrollWheel: false,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	
	map = new google.maps.Map(mapCanvas, mapOptions);
    directionsDisplay.setMap(map);
	//map.setCenter(latlon);

	var marker = new google.maps.Marker({
		position: latlon,
		map: map,
		draggable: true,
		title: "Current location!"
	});

	google.maps.event.addListener(marker, 'dragend', function(event) {
		var lat = this.position.lat();
		var lng = this.position.lng();
		latlon = new google.maps.LatLng(lat, lng);
		console.log("updated latlon: " + latlon);
	});
}

// get directions
function getDirections(request) {
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer();

    directionsDisplay.setMap(map);
    directionsDisplay.setPanel($('#directions'));

    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });

    $('#destinations').hide();
    $('#directions').show();
}

// get the map once the page is loaded
google.maps.event.addDomListener(window, 'load', getLocation);

// once the page is ready
$(document).ready(function() {
    var dests = $('#destinations');
    var i = $('#destinations li').length;

$(".mod").on("click", ".add-network-value", function (e) {
        var input = '<div class="input-group"><input type="text" class="autocomplete form-control"><span class="input-group-btn"><button class="btn btn-danger remove_dest" type="button" data-id="' + i + '">&times;</button></span></div>';
        var autocomp_opt = {
            source: ["pizza", "bar", "laundry","hosputal", "hotel", "shit", "park"]
        };
        var item = '<div class="networks-item"></div>';
        input = $(input).autocomplete(autocomp_opt);
        item = $(item).append(input);
        $(this).parents(".sub-content").append(item);
    });



    // adding destinations dynamically
    $('#add_dest').click(function() {
        if (i < 7) {
            i++;
            dests.append('<div class="input-group"><input type="text" class="autocomplete form-control"><span class="input-group-btn"><button class="btn btn-danger remove_dest" type="button" data-id="' + i + '">&times;</button></span></div>');
        } else {
            $("#max_dest").removeClass("hidden");
        }
        return false;
    });

   	// removing destinations
    $(document).on('click', '.remove_dest', function() {
        var id = $(this).data("id");
        $("*[data-id=" + id + "]").parent().parent().remove();
        i--;
        $("#max_dest").addClass("hidden");
    });

    $('#calculate').click(function(e) {
        //reset global Arrays
        res = new Array();
        res_arr = new Array();

    	e.preventDefault();
    	var dests = $('#destinations');
    	var i = $('#destinations li').length;

    	// get the transportation mode type from html
		if(document.getElementById('driving').checked) {
			mode = google.maps.TravelMode.DRIVING;
		} else if(document.getElementById('walking').checked) {
			mode = google.maps.TravelMode.WALKING;
		} else if(document.getElementById('bicycle').checked) {
			mode = google.maps.TravelMode.BICYCLING;
		}

		// get the radius from html
    	var sel = document.getElementById('radius');
    	//rad = sel.options[sel.selectedIndex].value;

    	console.log("Got here!\n");
    	var data_items = {};

    	var index = 0;
    	var items = dests.find("input");

    	// CONVERT miles to meters
    	console.log("Radius in miles: " + rad);
    	rad = 175; // rad/0.00062137
    	console.log("Radius in meters: " + rad);

    	console.log("Radius: " + rad);

    	data_items["location"] = latlon;
    	data_items["radius"] = rad;
        data_items["mode"] = mode;
        data_items["params"] = items.length;

        console.log(items);

        var params = [];
        res_arr[0] = [latlon];
        for(var v = 0; v < items.length; v++) {
            res_arr[v+1] = new Array(); 
    		var val = $(items[v]).val();
    		console.log(val);
    		data_items[index++] = val;

    		// console.log("latlon inside param: " + latlon);
    		var param = {
    		    location: new google.maps.LatLng(lat, lng),
    		    radius: rad,
    		    mode: mode,
    		    keyword: val,
                query: val
    		};

    		params.push(param);
    	};

        /* $.post('/data', data_items, function(returnedData) {
    		console.log(returnedData);
    	}); */

		var service = new google.maps.places.PlacesService(map);
    	var count = 0;

        // promises call for radar search
    	var promises = params.map(function(name) {
            return service.nearbySearch(name, function(results, status) {
                res.push(results);
                for (var i = 0; i < results.length; i++) {
                    createMarker(results[i]);
                }

                count++;
                if (count == params.length) {

                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({'latLng': latlon}, function(results, status) {
                        start_address = {
                            vicinity: results[0].formatted_address
                        };

                        // add start address to beginning of array
                        res.unshift([start_address]);

                        // create combinations of locations
                        var combos = combination(res);

                        // prepar DM for distance matrix
                        combos.forEach(function(combo) {
                           tmp = [];
                           combo.forEach(function(obj) {
                                tmp.push(obj.vicinity);
                           });
                           DM.push(tmp);
                        });

                        console.log(DM);

                        // call distance matrix on all combinations
                        DM.forEach(function(combo) {
                            distanceMatrix(combo, mode, DM.length);
                            sleep(2000);
                        });
                    });

                    /* combos = combination(res);

                    combos.forEach(function(combo, index) {
                        distanceMatrix(combo, mode, combos.length);
                    }); */
                }
            });
        });
    });
});

function createMarker(place) {
	var placeLoc = place.geometry.location;
	var marker = new google.maps.Marker({
		map: map,
		position: placeLoc,
		title: place.name
	});
}

function cartesianProductOf() {
    return _.reduce(arguments, function(a, b) {
        return _.flatten(_.map(a, function(x) {
            return _.map(b, function(y) {
                return x.concat([y]);
            });
        }), true);
    }, [ [] ]);
};

// http://stackoverflow.com/questions/12303989/cartesian-product-of-multiple-arrays-in-javascript

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}