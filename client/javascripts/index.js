var lat, lng;
var map, mode, radius;
var counter=0;
var API_ENDPOINT, CLIENT_ID, CLIENT_SECRET;

window.addEventListener('load', getLocation);

// code for html5 geolocation
function getLocation()
{
    if (navigator.geolocation)
    {
        // timeout at 10 seconds or 10000 milliseconds
        console.log("Looks like i got the location!");
        var timelimit = {
            timeout: 10000
        };
        navigator.geolocation.getCurrentPosition(showPosition, showError, timelimit);
    }
    else
    {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

// show the user's location
function showPosition(position)
{
    /*
    lat = 40.7143528;
    lng = -74.0059731;
    */
    lat = position.coords.latitude;
    lng = position.coords.longitude;

    console.log("function: showPosition: Latitude: " + lat + " Longitude: " + lng);
    prepareMap(lat, lng);
}

// show the default location
function showError(error)
{
    lat = 40.7143528;
    lng = -74.0059731;

    console.log("function: showError: Latitude: " + lat + " Longitude: " + lng);
    prepareMap(lat, lng);
}

// pre-condition: places is an array of stings, tag is a string
function match_input(places, tag)
{
    var lowcase_places = new Array();
    var entry = -1;
    //turn the input array and the tag into lowcase
    for (var i = 0; i < places.length; i++)
    {
        lowcase_places[i] = places[i].toLowerCase();
    }
    var lowcase_tag = tag.toLowerCase();

    //start to match 
    var i;
    for (i = 0; i < lowcase_places.length; i++)
    {

        if (lowcase_tag.indexOf(lowcase_places[i]) >= 0)
        {
            // if we found the substring
            return i;
        }
    }

    // entry is where the tag should fit in
    return entry;
}

function group(venues, places)
{
    //console.log(venues.length);
    var osm_array = new Array();
    var cate_num = places.length;
    var lat_lng = '';
    var lat;
    var lng;
    var threshold = 10;

    //initiate a an array of arrays
    for (var i = 0; i < cate_num; i++)
    {
        osm_array[i] = new Array();
    }

    var result = -1;
    for (var i = 0; i < venues.length; i++)
    {
        result = match_input(places, venues[i].categories[0].name);
        
        if (result != -1 && osm_array[result].length < threshold)
        {
            lat = venues[i].location.lat;
            lng = venues[i].location.lng;
            lat_lng = lat_lng.concat(lat, ',', lng);
            osm_array[result].push(
                lat_lng
            );
            lat_lng = '';
        }
    }
    return osm_array;
}

function expand_direction(code)
{
    var direction;
    switch (code)
    {
        case 'N':direction = 'North';break;
        case 'S':direction = 'South';break;
        case 'W':direction = 'West';break;
        case 'E':direction = 'East';break;
        default: direction = 'straight';
    }
    return direction;
}

function prepareMap(lat, lng)
{
    console.log("function: prepareMap: Latitude: " + lat + " Longitude: " + lng);

/*
    lat = 40.7143528;
    lng = -74.0059731;
    */

    var latlng = [lat, lng];

    console.log("latlng: " + latlng[0] + " " + latlng[1]);

    L.mapbox.accessToken = 'pk.eyJ1Ijoic3VrZXR1OTMiLCJhIjoiVDRNNndyTSJ9.5oW805KlbqhxiCsqoxnDiA';
    map = L.mapbox.map('map', 'suketu93.l62ii624').setView(latlng, 13);

    // Credit Foursquare for their wonderful data
    map.attributionControl.addAttribution('<a href="https://foursquare.com/">Places data from Foursquare</a>');

    // Create a Foursquare developer account: https://developer.foursquare.com/
    CLIENT_ID = 'QGPZS0XNT5CHNLUJYLXV35VRKEE35WPMYIOSJQDCPPPHVSCY';
    CLIENT_SECRET = 'KXO0YJHHZVSEFNZIRURWHTVYOZXEVTLZJQAOE0QJ0LZXJPVM';

    var myLayer = L.mapbox.featureLayer().addTo(map);

    myLayer.setGeoJSON(
    {
        type: 'Feature',
        geometry:
        {
            type: 'Point',
            coordinates: [lng, lat]
        },
        properties:
        {
            'title': 'Here I am!',
            'marker-color': '#FE2E2E',
            'marker-symbol': 'star'
        }
    });

// just testing ploylines
    

    // map.fitBounds(foursquarePlaces.getBounds());
}

//extract the distance table from 'data'


// combination.js
function str2arr(str)
{
    var arr = new Array();
    arr = str.split("/");
    return arr;
}

function cross_product(list1, list2)
{
    var result = new Array();
    var pointer = 0;
    var length1 = list1.length;
    var length2 = list2.length;
    for (var i = 0; i < length1; i++)
    {

        for (var j = 0; j < length2; j++)
        {
            //result[pointer]= new Array();
            //result[pointer] = [list1[i],"/",list2[j]];
            result[pointer] = list1[i].concat("/", list2[j]);
            pointer++;
        }
    }
    return result;
}

function combination(M)
{
    var r = new Array();
    r = M[0];
    for (var i = 1; i < M.length; i++)
    {
        r = cross_product(r, M[i]);
    }

    for (var i = 0; i < r.length; i++)
    {
        r[i] = str2arr(r[i]);
    }
    return r;
}

function search(place, DM)
{
  var i=0;
  for(i =0; i< DM.places.length; i++)
  {
    if(DM.places[i] == place)
      return i;
  }
}

var solution = new Array();
var min_cost = 198112;

function permutation(list,  k,  m,  DM)
{
    // generate all permutation of the list
    // list: all the places in the current combo , immutable
    // k : the recursion controler
    // m:  the recursion controler, immutable
    // DM: the distance matrix with two values--------place list and the table
    // min: records the minimum value of the route cost, updated when a cost is lower than it

    var i; 
    var sum=0;
    var from;
    var to;

    //var solution = new Array();
    
    if (k == m) { 
      // to traverse the list
        for (i = 0; i <= m; i++)  
            for(var j=0,sum=0; j< list.length-1; j++)
              {
                from = search(list[j], DM);
                to= search(list[j+1], DM);
                sum += DM.table[from][to];
                }
            // TO SUM UP THE COST!!!!!
            //console.log("sum is: ",sum);

        if(sum < min_cost)
        {
          min_cost = sum;
          //console.log('min updated: '+ sum);
          solution = list;
        }
    } 
    else  
      for (i=k; i <= m; i++) {  
      var temp;
      //swap list[k] and list[i];
      temp = list[k];
      list[k] = list[i];
      list[i] = temp;
             
      permutation(list, k+1, m, DM); 
      
      //do the swap again
      temp = list[k];
      list[k] = list[i];
      list[i] = temp; 
      } 

      // return our optimal solution (an array)
      //return min_cost;
      return solution;
}

// once the page is ready
$(document).ready(function()
{
    var dests = $('#destinations');
    var i = $('#destinations li').length;

    // adding destinations dynamically
    $('#add_dest').click(function()
    {
        if (i < 9)
        {
            i++;
            dests.append('<div class="input-group"><input type="text" class="form-control"><span class="input-group-btn"><button class="btn btn-danger remove_dest" type="button" data-id="' + i + '">&times;</button></span></div>');
        }
        else
        {
            $("#max_dest").removeClass("hidden");
        }
        return false;
    });

    // removing destinations
    $(document).on('click', '.remove_dest', function()
    {
        var id = $(this).data("id");
        $("*[data-id=" + id + "]").parent().parent().remove();
        i--;
        $("#max_dest").addClass("hidden");
    });

    var list = new Array();
    var t = 0;

    var foursquarePlaces = new L.LayerGroup();

    $('#calculate').click(function(e)
    {
        e.preventDefault();
        /*
        var origin = $('#origin'); 
        origin.after('<div class="well well-sm"><span class="label label-danger">origin</span> <div>[address information]</div></div>');
        */
        var dests = $('#destinations');
        var i = $('#destinations li').length;

        // get the transportation mode type from html
        /*if (document.getElementById('driving').checked)
        {
            mode = document.getElementById('driving').value;
        }
        else if (document.getElementById('walking').checked)
        {
            mode = document.getElementById('walking').value;
        }
        else if (document.getElementById('bicycle').checked)
        {
            mode = document.getElementById('bicycle').value;
        }*/

        // get the radius from html
        var sel = document.getElementById('radius');
        radius = sel.options[sel.selectedIndex].value;

        // CONVERT radius from miles to meters
        console.log("Radius in miles: " + radius);
        radius = radius / 0.00062137;
        console.log("Radius: " + radius);

        // keywords from the map
        var items = dests.find("input");

        var index = 0;
        var places = new Array();

        for (var v = 0; v < items.length; v++)
        {
            var val = $(items[v]).val();
            console.log("index: " + index + " value: " + val);
            places[index++] = val;
        }

        if (foursquarePlaces.layerGroup)
        {
            foursquarePlaces.clearLayers();
        }

        // Keep our place markers organized in a nice group.
        foursquarePlaces.addTo(map);

        var count = 0;
        var hook_counter=0;
        var address_hook_counter=0;
        var marker_color = new Array();
        var response_counter = 0;

        // Use jQuery to make an AJAX request to Foursquare to load markers data.
        for (var v = 0; v < places.length; v++)
        {
            var keyword = places[v];
            console.log("keyword: " + keyword);
            
            if(v == 0)
            {
                // initiate the stops well
                var first_stop = $('#stops');
                first_stop.after('<div class="well well-sm"><span class="label label-success">Stop 1</span><ul id= address_hook'+address_hook_counter+'></ul><div><ul id ="stops_hook'+hook_counter+'"></ul></div></div>');
                address_hook_counter++;
                hook_counter++;
            }
            else
            {
                // append on the stop hook
                hook_counter--;
                var other_stops = $('#stops_hook'+hook_counter);
                hook_counter++;
                var V= v+1;
                other_stops.after('<div class="label label-success">Stop'+ V+'</div><div><ul id= address_hook'+address_hook_counter+'></ul><ul id ="stops_hook'+hook_counter+'"></ul></div>');
                address_hook_counter++;
                hook_counter++;
            }
            

            // https://developer.foursquare.com/start/search
            API_ENDPOINT = 'https://api.foursquare.com/v2/venues/search' +
                '?client_id=CLIENT_ID' +
                '&client_secret=CLIENT_SECRET' +
                '&v=20130815' +
                '&ll=LATLON' +
                '&query=KEYWORD' +
                '&radius=RADIUS' +
                '&callback=?';

            var venues = [];

            $.getJSON(API_ENDPOINT
                .replace('CLIENT_ID', CLIENT_ID)
                .replace('CLIENT_SECRET', CLIENT_SECRET)
                .replace('KEYWORD', keyword)
                .replace('RADIUS', radius)
                .replace('LATLON', lat +
                    ',' + lng),
                function(result, status)
                {
                    if (status !== 'success')
                        return alert('Request to Foursquare failed');

                    response_counter++;

                    // Transform each venue result into a marker on the map.
                    for (var i = 0; i < result.response.venues.length; i++)
                    {
                        var venue = result.response.venues[i];
                        var latlng = L.latLng(venue.location.lat, venue.location.lng);
                        
                        //console.log('this is a '+venue.categories[0].name);
                        //console.log(venue);
                        venues.push(venue);
                        list[t] = venue;
                        t++;

                        // try using other ways to create markers
                        var myIcon = L.icon({
                        iconUrl: venue.categories[0].icon.prefix + 'bg_32' + venue.categories[0].icon.suffix,
                        iconSize: [22, 22],
                    });

                    L.marker([venue.location.lat, venue.location.lng], {icon: myIcon }).bindPopup('<strong><a href="https://foursquare.com/v/' + venue.id + '">' +
                                venue.name + '</a></strong>').addTo(foursquarePlaces);


                        /* for later use, display the list of icons */
                        var icons = $('#icons');
                        icons.append('<span><img src="' + venue.categories[0].icon.prefix + 'bg_32' + venue.categories[0].icon.suffix + '"</span>');
                        //icons.append('<span><img src="https://foursquare.com/img/categories_v2/food/icecream_bg_32.png"</span>');
                    }

                    if (response_counter == places.length)
                    {
                        var osm_array = group(list, places);
                        //console.log("osm_array: ")
                        //console.log(osm_array);
                        
                        var combos = combination(osm_array);
                        //console.log("combos: ")
                        

                       // console.log(combos);
                        
                        var host = "localhost";
                        var port = "5000";
                        
                        // TODO: parse combos and for each combo in combos, pass it to the distance table
                        combos.forEach(function(combo) {
                            var path = "/table?"
                            for(var i = 0; i < combo.length; i++) 
                            {
                                if(i == 0) 
                                {
                                    path += "loc=" + combo[i];
                                }
                                else 
                                {
                                    path += "&loc=" + combo[i];
                                }

                            }
                            
                            var theUrl = "http://" + host + ":" + port + path;
                            //console.log(theUrl);

                            var result;

                            $.post( "/data", {"url": theUrl}, function(data) {
                              
                              var table = JSON.parse(data);
                              //console.log('combo is: '+ combo);
                              var distance_matrix={
                                table: table.distance_table,
                                places:combo
                              }
                              //console.log('iteration number: '+counter);
                              counter++;
                              //console.log(distance_matrix);
                              
                              
                              var result= permutation(combo,0,combo.length-1,distance_matrix);
                              /*
                              console.log('best solution is: '+result);
                              console.log('min is: '+ min_cost);
                              console.log('-----');
                              */
                              
                              if(counter== combos.length)
                              {
                                console.log('The overall best solution is: '+result);
                                console.log('The overall minimum cost is: '+min_cost);
                                
                                // pend the informaition on small window
                                var hooks = new Array();
                                var circle_options = {
                                    color: '#FF0040',      // Stroke color
                                    
                                };

                                //prepare for routing
                                var line_points = new Array();
                                resulting = new Array();
                                for(var i=0; i< result.length; i++)
                                {
                                    var r = result[i].split(',');
                                    console.log(r);
                                    resulting.push(r);
                                    var circle_one = L.circle(r, 40, circle_options).addTo(foursquarePlaces);
                                }

                                //console.log(line_points);
                                //drawing circles
                                
                                
                                
                                //console.log(venues[0]);
                                hook_counter = 0;
                                console.log(resulting.length);
                                for(var x = 0; x < resulting.length; x++) {
                                    var viaurl = "http://" + host + ":" + port + "/viaroute?";
                                    if(x == 0) {
                                        viaurl += "loc=" + lat + "," + lng;
                                        viaurl += "&loc=" + resulting[x][0] + "," + resulting[x][1] + "&instructions=true&compression=false";
                                    }
                                    if(x != 0) {
                                        viaurl += "loc=" + resulting[x-1][0] + "," + resulting[x-1][1];
                                        viaurl += "&loc=" + resulting[x][0] + "," + resulting[x][1] + "&instructions=true&compression=false";
                                    }
                                    //console.log(viaurl);
                                    //console.log("x is: "+x);
                                    var ins_counter=0;
                                   $.post( "/data", {"url": viaurl}, function(dat) {
                                        var routes = JSON.parse(dat);
                                        console.log(routes);
                                        // draw polyline
                                        var polyline_options = {
                                            color: '#2E2EFE'
                                        };
                                        var polyline = L.polyline(routes.route_geometry, polyline_options).addTo(foursquarePlaces);
                                        
                                        // create a direction chunk
                                        // we discard the first element in route_instruction
                                       ins_counter=0;
                                       console.log('how many Instructions: '+ routes.route_instructions.length);

                                       
                                       for(ins_counter=routes.route_instructions.length-1; ins_counter>0; ins_counter--)
                                    {
                                       var wayname = 'continue on '+routes.route_instructions[ins_counter][1]+'<br>';
                                       var time = '('+ routes.route_instructions[ins_counter][4]+' seconds'+')'+'<br>';
                                       var length_in_unit = 'go '+ routes.route_instructions[ins_counter][5];
                                       var direction = 'Turn '+ expand_direction(routes.route_instructions[ins_counter][6])+' and'+'<br>';

                                       if(routes.route_instructions[ins_counter][1] == '')
                                        wayname = 'continue on the same road <br>';

                                       if(routes.route_instructions[ins_counter][4]==0 )
                                        {
                                            length_in_unit = 'You are there!';
                                            time = '<br>';
                                        }
                                
                                       var info_chunk = direction+wayname+length_in_unit+time;
                                        hooks[hook_counter] = $('#address_hook'+hook_counter);
                                        //hooks[hook_counter].after(info_chunk); // info_chunks
                                        var collapse_chunk = 
                                        '<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#stop'+hook_counter+'step'+ins_counter+'">step'+ins_counter+'</button>'+
                                        '<div id="stop'+hook_counter+'step'+ins_counter+'" class="collapse out">'+info_chunk+'</div>';
                                        hooks[hook_counter].after(collapse_chunk);  // collapse_chunk
                                    }

                                        // link the chunk to the window 
                                        hook_counter++;

                                        
                                    });

                                }
                                

                              }

                              

                            });
                        });
                    }
                });
        };
    });
});