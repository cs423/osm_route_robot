var express = require('express');
var path = require('path');
var client = express.Router();

/* GET home page. */
client.get('/', function(req, res) {
  res.render('index.html');
});

module.exports = client;